import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by yevhenii on 24.03.16.
 */
//JPanel is a generic lightweight container
public class Skeleton extends JPanel implements ActionListener{
    //area for drawing
    private int _maxWidth;
    private int _maxHeight;
    Timer timer;
    private int x;
    private int y;
    private Star star;
    private Stem stem;

    public Skeleton(int height, int width){
        this.setMaxHeight(height);
        this.setMaxWidth(width);
        timer = new Timer(1, this);
        timer.start();
    }

    //Getters & Setters for properties
    public int getMaxWidth(){
        return _maxWidth;
    }
    public int getMaxHeight(){
        return _maxHeight;
    }

    public void setMaxWidth(int width){
        if(width >= 0) {
            _maxWidth = width;
        }
        else{
            throw new IllegalArgumentException("Width cannot be negative.");
        }
    }

    public void setMaxHeight(int height){
        if(height >= 0) {
            _maxHeight = height;
        }
        else{
            throw new IllegalArgumentException("Height cannot be negative.");
        }
    }
    float agle = 0;
    /** Painting logic **/
    //Graphics - old awt library class, on which Java2D is based
    public void paint(Graphics g) {
        //type casting
        Graphics2D g2d = (Graphics2D) g;
        //setting up rendering quality
        clearDraw(g2d);
        //g2d.drawString("Hello, JAVA2D!", 10, 150);
        g2d.setBackground(Color.red);
        g2d.clearRect(0, 0, getMaxWidth(), getMaxHeight());
        g2d.drawRect(5, 5, _maxWidth - 10, _maxHeight - 10);

        //LINE JOINS: https://www.w3.org/TR/SVG/images/painting/linejoin.png
        //Constructor(startX, startY, height, width, color)
        g2d.setStroke(new BasicStroke(4, BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL));
        g2d.drawRect(5, 5, _maxWidth - 10, _maxHeight - 10);
        star = new Star(x, y, Color.yellow, new GradientPaint(1, 1, Color.orange, 10, 10, Color.magenta, true));
        stem = new Stem((int)star.getCenterX(), (int)star.getCenterY(), 120, 6, Color.yellow);
        g2d.rotate(agle, star.getCenterX(), star.getCenterY());
        stem.paint(g2d);
        star.paint(g2d);
        agle += 0.01;
    }

    public void clearDraw(Graphics2D g2d){
        RenderingHints rh = new RenderingHints(
            RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON
        );
        rh.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2d.setRenderingHints(rh);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(stem == null)
            return;
        if(stem.getEndPositionY() < _maxHeight - 10 && star.getLeft() == 10)
            y += 2;
        else if(star.getRight() < _maxWidth - 10 && stem.getEndPositionY() >= _maxHeight - 10)
        {
            x +=2;
        }
        else if(star.getTop() > 10 && star.getRight() >= _maxWidth - 10)
        {
            y -=2;
        }
        else if(star.getLeft() > 10){
            x -= 2;
        }

        repaint();
    }
}
