import java.awt.*;

/**
 * Created by yevhenii on 24.03.16.
 */
public class Stem {
    private int startX;
    private int startY;
    private int stemHeight;
    private int stemWidth;
    private Color stemColor;

    public Stem(int x, int y){
        startX = x;
        startY = y;
    }

    public Stem(int x, int y, int height, int width, Color color){
        this(x, y);
        stemHeight = height;
        stemColor = color;
        stemWidth = width;
    }

    public int getEndPositionY(){
        return startY + stemHeight;
    }

    public void paint(Graphics2D g2d){
        g2d.setColor(stemColor);
        g2d.setStroke(new BasicStroke(stemWidth, BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL));
        g2d.drawLine(startX, startY, startX, startY + stemHeight);
    }
}
