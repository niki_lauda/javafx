import java.awt.*;
import java.awt.geom.GeneralPath;

/**
 * Created by yevhenii on 24.03.16.
 */
public class Star {
    private int _xpoints[] = {30, 105, 130, 155, 230, 180,
            190, 130, 70, 80};
    private int _ypoints[] = {80, 80, 10, 80, 80, 125,
            190, 150, 190, 125};

    private GeneralPath star = new GeneralPath();
    private GeneralPath insidePolygon = new GeneralPath();

    private Color starColor;
    private GradientPaint polygonGradient;
    private int startX;
    private int startY;

    public Star(int x, int y, Color color, GradientPaint gradient){
        starColor = color;
        polygonGradient = gradient;
        startX = x;
        startY = y;
        init();
    }

    public int getRight(){
        int max = _xpoints[0];

        for (int i = 1; i < _xpoints.length; i++) {
            if (_xpoints[i] > max) {
                max = _xpoints[i];
            }
        }
        return max + startX;
    }
    public int getLeft(){
        int min = _xpoints[0];

        for (int i = 1; i < _xpoints.length; i++) {
            if (_xpoints[i] < min) {
                min = _xpoints[i];
            }
        }
        return min + startX;
    }
    public int getTop(){
        int min = _ypoints[0];

        for (int i = 1; i < _ypoints.length; i++) {
            if (_ypoints[i] < min) {
                min = _ypoints[i];
            }
        }
        return min + startY;
    }

    private void init(){
        star.moveTo(startX + _xpoints[0], startY + _ypoints[0]);
        for(int i = 1; i < _xpoints.length; i++){
            star.lineTo(startX + _xpoints[i],startY + _ypoints[i]);
        }
        star.closePath();

        insidePolygon.moveTo(startX + _xpoints[1], startY + _ypoints[1]);
        for(int i = 1; i < _xpoints.length; i+= 2){
            insidePolygon.lineTo(startX + _xpoints[i], startY + _ypoints[i]);
        }
        insidePolygon.closePath();



    }
    public void paint(Graphics2D g2d){

        g2d.setColor(starColor);
        g2d.fill(star);
        g2d.setPaint(polygonGradient);
        g2d.fill(insidePolygon);


    }

    public double getCenterX(){
        return star.getBounds().getCenterX();
    }

    public double getCenterY(){
        return star.getBounds().getCenterY();
    }

}
