import javax.swing.*;
import java.awt.*;

/**
 * Created by yevhenii on 24.03.16.
 */
public class Main {
    public static void main(String[] args){
        //An extended version of java.awt.Frame class that supports Swing components
        JFrame frame = new JFrame("Hello, Java 2d!");
        //Close on exit
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //Set window size
        frame.setSize(500, 500);
        //setting window appear on the center of display
        frame.setLocationRelativeTo(null);
        //make unresizeable
        frame.setResizable(false);

        //Encapsulation of the width and height in a single object
        Dimension size = frame.getSize();
        //Representation of the borders of the container
        Insets insets = frame.getInsets();

        //adding our container to the form and showing it
        frame.add(new Skeleton(
                size.height-insets.left - insets.right,
                size.width - insets.bottom - insets.top));
        frame.setVisible(true);
    }
}
