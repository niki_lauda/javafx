import com.sun.j3d.utils.geometry.Box;

import javax.media.j3d.Appearance;
import javax.media.j3d.Material;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.vecmath.Color3f;
import javax.vecmath.Vector3f;

/**
 * Created by yevhenii on 12.05.16.
 */
public class Griff {
    static TransformGroup getGriff(Vector3f coords){
        TransformGroup mainboxTransf = new TransformGroup();
        Transform3D transfMainBox = new Transform3D();
        Appearance ap = new Appearance();
        Color3f emissive = new Color3f(0.0f, 0.05f, 0.0f);
        Color3f ambient = new Color3f(0.2f, 0.5f, 0.15f);
        Color3f diffuse = new Color3f(0.2f, 0.15f, .15f);
        Color3f specular = new Color3f(0.502f, 0.000f, 0.000f);
        ap.setMaterial(new Material(ambient, emissive, diffuse, specular, 1.0f));
        Box mainBox = new Box(0.04f, 0.02f, 0.005f, ap);
        transfMainBox.setTranslation(coords);
        mainboxTransf.setTransform(transfMainBox);
        mainboxTransf.addChild(mainBox);
        return mainboxTransf;
    }
}
