import com.sun.j3d.utils.geometry.Cylinder;
import com.sun.j3d.utils.geometry.Primitive;
import com.sun.j3d.utils.image.TextureLoader;

import javax.media.j3d.*;
import javax.vecmath.Color3f;
import javax.vecmath.Color4f;
import javax.vecmath.Vector3f;
import java.awt.*;

/**
 * Created by yevhenii on 12.05.16.
 */
public class Tire {

    public static TransformGroup getTire(Vector3f coords){
        TransformGroup tireTransformGroup = new TransformGroup();
        Transform3D tireTransform = new Transform3D();
        TextureLoader loader = new TextureLoader("tire.png", "TIRE", new Container());
        Texture texture = loader.getTexture();
        texture.setBoundaryModeS(Texture.WRAP);
        texture.setBoundaryModeT(Texture.WRAP);
        texture.setBoundaryColor(new Color4f(0.0f, 0.0f, 0.0f, 0.0f));
        TextureAttributes texAttr = new TextureAttributes();
        texAttr.setTextureMode(TextureAttributes.MODULATE);
        Appearance ap = new Appearance();
        ap.setTexture(texture);
        ap.setTextureAttributes(texAttr);
        int primflags = Primitive.GENERATE_NORMALS + Primitive.GENERATE_TEXTURE_COORDS;
        Cylinder lamp = new Cylinder(0.1f, 0.1f, primflags, ap);
        tireTransform.rotX(Math.PI/2);
        tireTransform.setTranslation(coords);
        tireTransformGroup.setTransform(tireTransform);
        tireTransformGroup.addChild(lamp);
        return tireTransformGroup;
    }
}
