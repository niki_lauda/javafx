import com.sun.j3d.utils.geometry.Box;
import com.sun.j3d.utils.geometry.Cone;

import javax.media.j3d.Appearance;
import javax.media.j3d.Material;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.vecmath.Color3f;
import javax.vecmath.Vector3f;

/**
 * Created by yevhenii on 12.05.16.
 */
public class Lamp {
    static TransformGroup getLamp(Vector3f coords){
        TransformGroup groupTrans = new TransformGroup();
        Transform3D transfLamp = new Transform3D();
        Appearance ap = new Appearance();
        Color3f emissive = new Color3f(0.0f, 0.05f, 0.0f);
        Color3f ambient = new Color3f(0.2f, 0.5f, 0.15f);
        Color3f diffuse = new Color3f(0.2f, 0.15f, .15f);
        Color3f specular = new Color3f(1.000f, 1.000f, 0.000f);
        ap.setMaterial(new Material(ambient, emissive, diffuse, specular, 1.0f));
        Cone mainBox = new Cone(0.08f, 0.05f, ap);
        transfLamp.rotZ(Math.PI/2);
        transfLamp.setTranslation(coords);
        groupTrans.setTransform(transfLamp);
        groupTrans.addChild(mainBox);
        return groupTrans;
    }

}
