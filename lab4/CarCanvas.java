import com.sun.j3d.utils.geometry.Box;
import com.sun.j3d.utils.geometry.Primitive;
import com.sun.j3d.utils.image.TextureLoader;

import javax.media.j3d.*;
import javax.vecmath.Color3f;
import javax.vecmath.Color4f;
import javax.vecmath.Vector3f;
import java.awt.*;

/**
 * Created by yevhenii on 12.05.16.
 */
public class CarCanvas {
    static TransformGroup getCarCanvas(Vector3f coords){
        TransformGroup mainboxTransf = new TransformGroup();
        Transform3D transfMainBox = new Transform3D();
        TextureLoader loader = new TextureLoader("texture.jpg", "LUMINANCE", new Container());
        Texture texture = loader.getTexture();
        texture.setBoundaryModeS(Texture.WRAP);
        texture.setBoundaryModeT(Texture.WRAP);
        texture.setBoundaryColor(new Color4f(0.0f, 1.0f, 1.0f, 0.0f));
        TextureAttributes texAttr = new TextureAttributes();
        texAttr.setTextureMode(TextureAttributes.MODULATE);
        Appearance ap = new Appearance();
        ap.setTexture(texture);
        ap.setTextureAttributes(texAttr);
        Color3f emissive = new Color3f(0.0f, 0.05f, 0.0f);
        Color3f ambient = new Color3f(0.2f, 0.5f, 0.15f);
        Color3f diffuse = new Color3f(0.2f, 0.15f, .15f);
        Color3f specular = new Color3f(0.647f, 0.165f, 0.165f);
        ap.setMaterial(new Material(ambient, emissive, diffuse, specular, 1.0f));
        int primflags = Primitive.GENERATE_NORMALS + Primitive.GENERATE_TEXTURE_COORDS;
        Box mainBox = new Box(0.5f, 0.1f, 0.25f, primflags, ap);
        transfMainBox.setTranslation(coords);
        mainboxTransf.setTransform(transfMainBox);
        mainboxTransf.addChild(mainBox);
        return mainboxTransf;
    }

}
