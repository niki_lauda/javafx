import com.sun.j3d.utils.universe.SimpleUniverse;
import javax.media.j3d.*;
import javax.swing.*;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3f;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by yevhenii on 12.05.16.
 */
public class ComplexCar implements ActionListener {
    private TransformGroup carTransformGroup = new TransformGroup();;
    private Transform3D carTransform3D = new Transform3D();
    private Timer timer;
    private float angle = 0;

    public static void main(String[] args) {
            new ComplexCar();
    }

    public ComplexCar(){
        timer = new Timer(50, this);
        timer.start();
        BranchGroup scene = createSceneGraph();
        SimpleUniverse u = new SimpleUniverse();
        //setting viewing point on default
        u.getViewingPlatform().setNominalViewingTransform();
        u.addBranchGraph(scene);
    }

    public BranchGroup createSceneGraph(){
        //setting up our objects
        BranchGroup objRoot = new BranchGroup();
        buildCar();
        objRoot.addChild(carTransformGroup);

        //adding color
        Color3f lightColor = new Color3f(1f, 1f, 1f);
        BoundingSphere bounds = new BoundingSphere(new Point3d(0.0, 0.0, 0.0), 100.0);
        Vector3f lightDirection  = new Vector3f(4.0f, -7.0f, -12.0f);
        DirectionalLight light = new DirectionalLight(lightColor, lightDirection);
        light.setInfluencingBounds(bounds);
        objRoot.addChild(light);

        return objRoot;
    }

    public void buildCar(){
        //Specifies that the node allows writing its object's transform information.
        carTransformGroup.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        carTransformGroup.addChild(CarCanvas.getCarCanvas(new Vector3f(0f, 0f, 0f)));
        carTransformGroup.addChild(CarUpperCanvas.getCarCanvas(new Vector3f(0f, 0.15f, 0f)));
        carTransformGroup.addChild(Tire.getTire(new Vector3f(0.3f, -0.08f, 0.25f)));
        carTransformGroup.addChild(Tire.getTire(new Vector3f(0.3f, -0.08f, -0.25f)));
        carTransformGroup.addChild(Tire.getTire(new Vector3f(-0.3f, -0.08f, 0.25f)));
        carTransformGroup.addChild(Tire.getTire(new Vector3f(-0.3f, -0.08f, -0.25f)));
        carTransformGroup.addChild(Lamp.getLamp(new Vector3f(-0.5f, 0.02f, -0.16f)));
        carTransformGroup.addChild(Lamp.getLamp(new Vector3f(-0.5f, 0.02f, 0.16f)));
        carTransformGroup.addChild(gasPipe.getGasPipe(new Vector3f(0.5f, -0.04f, 0.16f)));
        carTransformGroup.addChild(Griff.getGriff(new Vector3f(0.1f, 0.03f, 0.25f)));
        carTransformGroup.addChild(Griff.getGriff(new Vector3f(0.1f, 0.03f, -0.25f)));

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        carTransform3D.rotY(angle);
        carTransformGroup.setTransform(carTransform3D);
        angle += 0.05;
    }
}