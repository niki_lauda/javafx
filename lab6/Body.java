import com.sun.j3d.utils.image.TextureLoader;

import javax.media.j3d.*;
import javax.vecmath.Color3f;
import javax.vecmath.Vector3d;

/**
 * Created by yevhenii on 22.05.16.
 */
public class Body {
    private Vector3d position;
    private Transform3D scale;
    private Transform3D transition;
    private Transform3D rotationBodyY;
    private Transform3D rotationBodyX;



    private Transform3D genericTransform;
    private Shape3D shape3D;

    public double getSizeCoeff() {
        return sizeCoeff;
    }

    public void setSizeCoeff(double sizeCoeff) {
        this.sizeCoeff = sizeCoeff;
    }

    private double sizeCoeff;
    private double rotateGradeY;
    private double rotateGradeX;

    public Body(Shape3D shape, Vector3d position, double sizeCoeff){
        this.shape3D = shape;
        this.position = position;
        this.sizeCoeff = sizeCoeff;

        this.shape3D.setAppearance(getAppearance());

        scale = new Transform3D();
        transition = new Transform3D();
        rotationBodyY = new Transform3D();
        rotationBodyX = new Transform3D();

        applyTransform();
    }

    public void rotateY(double grad){
        rotateGradeY += grad;
        applyTransform();
    }

    public void rotateX(double grad){
        rotateGradeX += grad;
        applyTransform();
    }

    public void move(Vector3d move){
        position = move;
        applyTransform();
    }

    private void applyTransform(){
        scale.setScale(getSizeCoeff());
        transition.setTranslation(position);
        rotationBodyY.rotY(Math.toRadians(rotateGradeY));
        rotationBodyX.rotZ(Math.toRadians(rotateGradeX));

        genericTransform = new Transform3D();
        genericTransform.mul(scale);
        genericTransform.mul(transition);
        genericTransform.mul(rotationBodyY);
        genericTransform.mul(rotationBodyX);

    }

    private Appearance getAppearance(){
        TextureLoader texture;
        texture = new TextureLoader("res/img/Body diff MAP.jpg", null);
        Material material = new Material();
        material.setAmbientColor ( new Color3f( 0, 0, 0 ) );
        material.setDiffuseColor ( new Color3f( 0, 0, 0 ) );
        material.setSpecularColor( new Color3f( 0, 0, 0) );
        material.setShininess( 0.3f );
        material.setLightingEnable(true);
        TextureAttributes texAttr = new TextureAttributes();
        texAttr.setTextureMode(TextureAttributes.REPLACE);
        Appearance app = new Appearance();
        app.setMaterial(material);
        app.setTextureAttributes(texAttr);
        app.setTexture(texture.getTexture());
        return app;
    }

    public Transform3D getGenericTransform() {
        return genericTransform;
    }

    public void setGenericTransform(Transform3D genericTransform) {
        this.genericTransform = genericTransform;
    }

    public Shape3D getShape3D() {
        return shape3D;
    }

    public void setShape3D(Shape3D shape3D) {
        this.shape3D = shape3D;
    }
}
