import com.sun.j3d.utils.geometry.*;
import com.sun.j3d.utils.geometry.Box;
import com.sun.j3d.utils.image.TextureLoader;
import com.sun.j3d.utils.universe.SimpleUniverse;

import javax.media.j3d.*;
import javax.swing.*;
import javax.vecmath.Color3f;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Created by yevhenii on 22.05.16.
 */
public class Environment extends JFrame implements KeyListener, ActionListener {
    Canvas3D canvas;
    SimpleUniverse universe;
    BranchGroup groupElems;
    BB8 bb8;
    Ground ground;
    boolean KEY_PRESSED = false;

    private TransformGroup camera;

    public Environment(){
        Timer timer = new Timer(0, this);
        timer.start();

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        canvas = new Canvas3D(SimpleUniverse.getPreferredConfiguration());
        canvas.addKeyListener(this);
        universe = new SimpleUniverse(canvas);
        universe.getViewingPlatform().setNominalViewingTransform();
        groupElems = new BranchGroup();

        addElements();

        setCamera();

        setTitle("Yevhenii Maliavka Lab 6");
        setSize(1350, 750);
        getContentPane().add("Center", canvas);
        setVisible(true);

    }

    private void setCamera(){
        camera = universe.getViewingPlatform().getViewPlatformTransform();
        universe.getViewingPlatform().setNominalViewingTransform();
        Transform3D setup = new Transform3D();
        setup.setTranslation(new Vector3d(0, 3f, 5.0f));
        Transform3D rotate = new Transform3D();
        rotate.rotX(Math.toRadians(-25));
        Transform3D generic = new Transform3D();
        generic.mul(setup);
        generic.mul(rotate);
        camera.setTransform(generic);
    }

    private void addElements(){
        bb8 = new BB8(new Vector3d(0.0f, 0.9f, 0f));
        ground = new Ground(new Vector3d(0, 0,0));
        groupElems.addChild(bb8.getTransformGroupBody());
        groupElems.addChild(bb8.getTransformGroupHead());
        groupElems.addChild(ground.getTransformGroupGround());
        groupElems.addChild(bb8.getTransformGroupLamp());

        int primflags = Primitive.GENERATE_NORMALS + Primitive.GENERATE_TEXTURE_COORDS;
        Vector3f size = new Vector3f(0.2f, 0.9f, 0.2f);
        Transform3D transform3D = new Transform3D();
        transform3D.setTranslation(new Vector3d(1.7, 1, -0.6f));
        Transform3D transform3D2 = new Transform3D();
        transform3D2.setTranslation(new Vector3d(-1.5, 1, 0.9f));
        Box box = new Box(size.x, size.y, size.z, primflags, getAppearance());
        Box box1 = new Box(size.x, size.y, size.z, primflags, getAppearance());
        TransformGroup gr = new TransformGroup();
        gr.setTransform(transform3D);
        TransformGroup gr2 = new TransformGroup();
        gr2.setTransform(transform3D2);

        gr.addChild(box);
        gr2.addChild(box1);
        groupElems.addChild(gr);
        groupElems.addChild(gr2);

        //groupElems.addChild(getLightToUniverse());
        universe.addBranchGraph(groupElems);
    }


    private Appearance getAppearance(){
        TextureLoader texture;
        texture = new TextureLoader("res/img/head top diff MAP.jpg", null);
        Material material = new Material();
        material.setAmbientColor ( new Color3f( 0.56f, 0.56f, 0.56f ) );
        material.setDiffuseColor ( new Color3f( 0.56f, 0.56f, 0.56f ) );
        material.setSpecularColor( new Color3f( 1f, 1, 1) );
        material.setShininess( 0.3f );
        material.setLightingEnable(true);
        TextureAttributes texAttr = new TextureAttributes();
        texAttr.setTextureMode(TextureAttributes.COMBINE);
        Appearance app = new Appearance();
        app.setMaterial(material);
        app.setTextureAttributes(texAttr);
        app.setTexture(texture.getTexture());
        return app;
    }

    private DirectionalLight getLightToUniverse() {
        BoundingSphere bounds = new BoundingSphere();
        bounds.setRadius(200);
        Color3f color = new Color3f(1.0f, 1f, 1f);
        //Vector3f lightDirection = new Vector3f(-100f, -100f, 0.0f);
        Vector3f lightDirection = new Vector3f(-3, 0f, 0.0f);

        DirectionalLight dirLight = new DirectionalLight(color, lightDirection);
        dirLight.setInfluencingBounds(bounds);
        return dirLight;
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if(KEY_PRESSED){
            return;
        }
        KEY_PRESSED = true;
        Vector3d dir;
        switch (e.getKeyChar()) {
            case 'w':
                if(-6.72 < bb8.getPosition().z)
                    bb8.accelerate(-0.08);
                break;
            case 'a':
                if(-10 < bb8.getPosition().x)
                    bb8.turn(-0.08);
                break;
            case 's':
                if(6.72 > bb8.getPosition().z)
                    bb8.accelerate(0.08);
                break;
            case 'd':
                if(10 > bb8.getPosition().x)
                    bb8.turn(0.08);
                break;
            case 'q':
                bb8.rotateHead(6);
                break;
            case 'e':
                bb8.rotateHead(-6);
                break;
            case 'r':
                bb8.lightLight(false);
                break;
            case 't':
                bb8.lightLight(true);
                break;
        }
        KEY_PRESSED = false;

    }


    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    public void actionPerformed(ActionEvent e) {
    }

}
