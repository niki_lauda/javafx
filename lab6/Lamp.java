import com.sun.j3d.utils.image.TextureLoader;

import javax.media.j3d.*;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;

/**
 * Created by yevhenii on 22.05.16.
 */
public class Lamp {
    private Vector3d position;
    private Transform3D genericTransform;
    private Transform3D rotationLight;
    private Transform3D transitionLight;


    private double rotateGrade;

    public double getSizeCoeff() {
        return sizeCoeff;
    }

    public void setSizeCoeff(double sizeCoeff) {
        this.sizeCoeff = sizeCoeff;
    }

    private double sizeCoeff;


    public DirectionalLight getLight() {
        return light;
    }

    public void setLight(DirectionalLight light) {
        this.light = light;
    }

    private DirectionalLight light;

    public Lamp(Vector3d position, double sizeCoeff) {
        this.position = position;
        this.sizeCoeff = sizeCoeff;

        rotationLight = new Transform3D();
        transitionLight = new Transform3D();
        light = getLightToUniverse();
        light.setCapability(DirectionalLight.ALLOW_DIRECTION_READ);
        light.setCapability(DirectionalLight.ALLOW_DIRECTION_WRITE);
        light.setCapability(DirectionalLight.ALLOW_STATE_READ);
        light.setCapability(DirectionalLight.ALLOW_STATE_WRITE);

        applyTransform();
    }

    private DirectionalLight getLightToUniverse() {
        BoundingSphere bounds = new BoundingSphere();
        bounds.setRadius(3);
        Color3f color = new Color3f(1.0f, 1f, 1f);
        Vector3f lightDirection = new Vector3f(-100, 0, 0f);
        DirectionalLight dirLight = new DirectionalLight(color, lightDirection);
        dirLight.setInfluencingBounds(bounds);
        return dirLight;
    }


    private void applyTransform() {
        rotationLight.rotY(Math.toRadians(rotateGrade));
        transitionLight.setTranslation(position);

        genericTransform = new Transform3D();
        genericTransform.mul(transitionLight);
        genericTransform.mul(rotationLight);
    }

    public void rotate(double grad) {
        rotateGrade += grad;
        Vector3f dir = new Vector3f();
//        light.getDirection(dir);
//        float angle =  (float)Math.toRadians(grad);
//        float newCordx = dir.x * (float)Math.cos(angle) - dir.y * (float)Math.sin(angle);
//        float newCordz = dir.y * (float)Math.cos(angle) + dir.x * (float)Math.sin(angle);
//        light.setDirection(newCordx, newCordz, dir.z );
        applyTransform();
    }

    public void move(Vector3d move) {
        position = move;
        applyTransform();
    }

    public Transform3D getGenericTransform() {
        return genericTransform;
    }

    public void setGenericTransform(Transform3D genericTransform) {
        this.genericTransform = genericTransform;
    }
}

