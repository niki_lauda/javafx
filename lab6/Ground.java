import com.sun.j3d.loaders.Scene;
import com.sun.j3d.loaders.objectfile.ObjectFile;
import com.sun.j3d.utils.geometry.Box;
import com.sun.j3d.utils.geometry.Primitive;
import com.sun.j3d.utils.image.TextureLoader;

import javax.media.j3d.*;
import javax.vecmath.Color3f;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by yevhenii on 22.05.16.
 */
public class Ground {
    private Vector3d position;
    private Box ground;

    public Vector3f getSize() {
        return size;
    }

    public void setSize(Vector3f size) {
        this.size = size;
    }

    private Vector3f size;


    public TransformGroup getTransformGroupGround() {
        return transformGroupGround;
    }

    public void setTransformGroupGround(TransformGroup transformGroupGround) {
        this.transformGroupGround = transformGroupGround;
    }

    private TransformGroup transformGroupGround;

    public Ground(Vector3d position){
        transformGroupGround = new TransformGroup();
        int primflags = Primitive.GENERATE_NORMALS + Primitive.GENERATE_TEXTURE_COORDS;

        size = new Vector3f(2f, 0.01f, 2f);
        ground = new Box(size.x, size.y, size.z, primflags, getAppearance());

        Transform3D rotate = new Transform3D();
        rotate.rotX(Math.PI);
        transformGroupGround.setTransform(rotate);
        transformGroupGround.addChild(ground);
    }

    private Appearance getAppearance(){
        TextureLoader texture;
        texture = new TextureLoader("res/img/head top diff MAP.jpg", null);
        Material material = new Material();
        material.setAmbientColor ( new Color3f( 0, 0, 0 ) );
        material.setDiffuseColor ( new Color3f( 0, 0, 0 ) );
        material.setSpecularColor( new Color3f( 0, 0, 0) );
        material.setShininess( 0.3f );
        material.setLightingEnable(true);
        TextureAttributes texAttr = new TextureAttributes();
        texAttr.setTextureMode(TextureAttributes.REPLACE);
        Appearance app = new Appearance();
        app.setMaterial(material);
        app.setTextureAttributes(texAttr);
        app.setTexture(texture.getTexture());
        return app;
    }

    private static Scene loadScene(String location) throws IOException{
        ObjectFile loader = new ObjectFile(ObjectFile.RESIZE);
        return loader.load(new FileReader(location));
    }
}
