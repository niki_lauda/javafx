import com.sun.j3d.loaders.Scene;
import com.sun.j3d.loaders.objectfile.ObjectFile;

import javax.media.j3d.Shape3D;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.vecmath.Vector3d;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;

/**
 * Created by yevhenii on 22.05.16.
 */
public class BB8 {
    private Vector3d direction;

    public Vector3d getPosition() {
        return position;
    }

    public void setPosition(Vector3d position) {
        this.position = position;
    }

    private Vector3d position;

    public TransformGroup getTransformGroupBody() {
        return transformGroupBody;
    }

    public void setTransformGroupBody(TransformGroup transformGroupBody) {
        this.transformGroupBody = transformGroupBody;
    }

    private TransformGroup transformGroupBody;

    public TransformGroup getTransformGroupHead() {
        return transformGroupHead;
    }

    public void setTransformGroupHead(TransformGroup transformGroupHead) {
        this.transformGroupHead = transformGroupHead;
    }

    private TransformGroup transformGroupHead;

    public TransformGroup getTransformGroupLamp() {
        return transformGroupLamp;
    }

    public void setTransformGroupLamp(TransformGroup transformGroupLamp) {
        this.transformGroupLamp = transformGroupLamp;
    }

    private TransformGroup transformGroupLamp;


    private Transform3D genericTransform;

    private Body body;
    private Head head;
    private Lamp lamp;
    boolean showLight = true;
    private double sizeCoeff = 0.2;
    private double rotateGrade;

    public BB8(Vector3d position){
        this.position = position;
        loadShapes();
    }

    private void loadShapes(){
        try{
            Scene scene = loadScene("res/bb8.obj");
            listSceneNamedObjects(scene);

            Shape3D shape = (Shape3D)scene.getNamedObjects().get("head");

            head = new Head(shape, new Vector3d(position.x, position.y + 0.025f, position.z), sizeCoeff);

            scene.getSceneGroup().removeChild(shape);
            Shape3D shape2 = (Shape3D)scene.getNamedObjects().get("body");
            body = new Body(shape2, new Vector3d(position.x, position.y, position.z), sizeCoeff);
            scene.getSceneGroup().removeChild(shape);
            scene.getSceneGroup().removeAllChildren();
        }
        catch(IOException e){
            System.out.println(e.getCause());
        }


        lamp = new Lamp(new Vector3d(position.x + 0.5, position.y+ 0.5f, position.z), sizeCoeff);


        transformGroupBody = new TransformGroup();
        transformGroupBody.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        transformGroupBody.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        transformGroupBody.addChild(body.getShape3D());



        transformGroupHead = new TransformGroup();
        transformGroupHead.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        transformGroupHead.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        transformGroupHead.addChild(head.getShape3D());

        transformGroupLamp = new TransformGroup();
        transformGroupLamp.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        transformGroupLamp.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        transformGroupLamp.addChild(lamp.getLight());

        applyHeadTransform();
        applyBodyTransform();
        applyLampTransform();


    }

    private void applyLampTransform(){

        genericTransform = new Transform3D();
        transformGroupLamp.setTransform(lamp.getGenericTransform());
    }


    private void applyHeadTransform(){
        genericTransform = new Transform3D();
        transformGroupHead.setTransform(head.getGenericTransform());
    }

    private void applyBodyTransform(){
        genericTransform = new Transform3D();
        transformGroupBody.setTransform(body.getGenericTransform());
    }



    public void rotateHead(double grad){
        head.rotate(grad);
        lamp.rotate(grad);
        applyHeadTransform();
        applyLampTransform();
    }

    public void turn(double step){
        position = new Vector3d(position.x + step , position.y , position.z);
        body.move(position);
        head.move(position);
        lamp.move(new Vector3d(position.x, position.y + 0.5, position.z));


        body.rotateY(Math.signum(step) * 15);

        applyHeadTransform();
        applyBodyTransform();
        applyLampTransform();
    }

    public void lightLight(boolean value){
        showLight = value;
        if(!showLight){
            lamp.getLight().setEnable(false);
            return;
        }
        lamp.getLight().setEnable(true);
    }

    public void accelerate(double step){
        position = new Vector3d(position.x, position.y, position.z + step);
        body.move(position);
        head.move(position);
        lamp.move(new Vector3d(position.x, position.y + 0.5, position.z));

        body.rotateY(Math.signum(step) * 15);

        applyHeadTransform();
        applyBodyTransform();
        applyLampTransform();
    }


    private void listSceneNamedObjects(Scene scene){
        if(scene == null){
            return;
        }
        Map<String, Shape3D> nameMap = scene.getNamedObjects();

        for(String name : nameMap.keySet()){
            System.out.printf("Name: %s\n", name);
        }
    }

    private static Scene loadScene(String location) throws IOException{
        ObjectFile loader = new ObjectFile(ObjectFile.RESIZE);
        return loader.load(new FileReader(location));
    }
}
