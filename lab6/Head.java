import com.sun.j3d.utils.image.TextureLoader;

import javax.media.j3d.*;
import javax.vecmath.Color3f;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;

/**
 * Created by yevhenii on 22.05.16.
 */
public class Head {
    private Vector3d position;
    private Transform3D scale;
    private Transform3D transition;
    private Transform3D rotationHead;
    private Transform3D genericTransform;



    private double rotateGrade;

    public double getSizeCoeff() {
        return sizeCoeff;
    }

    public void setSizeCoeff(double sizeCoeff) {
        this.sizeCoeff = sizeCoeff;
    }

    private double sizeCoeff;


    private Shape3D shape3D;


    public Head(Shape3D shape, Vector3d position, double sizeCoeff){
        this.shape3D = shape;
        this.shape3D.setAppearance(getAppearance());
        this.position = position;
        this.sizeCoeff = sizeCoeff;

        scale = new Transform3D();
        transition = new Transform3D();
        rotationHead = new Transform3D();
        applyTransform();
    }

    private Appearance getAppearance(){
        TextureLoader texture;
        texture = new TextureLoader("res/img/Head ring diff MAP.jpg", null);
        Material material = new Material();
        material.setEmissiveColor(new Color3f(0.9f, 0.9f, 0.9f));
        material.setAmbientColor ( new Color3f(0.9f, 0.9f, 0.9f ) );
        material.setDiffuseColor ( new Color3f( 0.9f, 0.9f, 0.9f) );
        material.setSpecularColor( new Color3f(  0.9f, 0.9f, 0.9f ) );
        material.setShininess( 0.3f );
        material.setLightingEnable(true);
        TextureAttributes texAttr = new TextureAttributes();
        texAttr.setTextureMode(TextureAttributes.COMBINE);
        Appearance app = new Appearance();
        app.setMaterial(material);
        app.setTextureAttributes(texAttr);
        app.setTexture(texture.getTexture());
        return app;
    }


    private void applyTransform(){
        scale.setScale(getSizeCoeff());
        rotationHead.rotY(Math.toRadians(rotateGrade));
        transition.setTranslation(position);
        genericTransform = new Transform3D();
        genericTransform.mul(scale);
        genericTransform.mul(transition);
        genericTransform.mul(rotationHead);
    }

    public void rotate(double grad){
        rotateGrade += grad;
        applyTransform();
    }
    public void move(Vector3d move){
        position = move;
        applyTransform();
    }

    public Transform3D getGenericTransform() {
        return genericTransform;
    }

    public void setGenericTransform(Transform3D genericTransform) {
        this.genericTransform = genericTransform;
    }

    public Shape3D getShape3D() {
        return shape3D;
    }

    public void setShape3D(Shape3D shape3D) {
        this.shape3D = shape3D;
    }
}
