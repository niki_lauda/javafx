import javafx.animation.*;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.ToolBar;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.scene.shape.*;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.Time;
import java.util.ArrayList;

import javafx.util.Duration;

class Point{
    public int x;
    public int y;

    public Point(int _x, int _y){
        x = _x;
        y = _y;
    }
}

public class PrintingImage extends Application {

    private static final int SCENE_WIDTH = 1100;
    private static final int SCENE_HEIGHT = 700;
    private static String DEFAULT_FILE = "/home/yevhenii/src/java/lab3/testImages/bmp2.bmp";
    private  char[][] map;
    private ArrayList<Point> list = new ArrayList<Point>();
    private HeaderBitmapImage image;
    private int numberOfPixels;
    Duck duck = new Duck();
    public PrintingImage() {
    }

    public PrintingImage(HeaderBitmapImage image) {
        this.image = image;
    }

    public void start(Stage primaryStage){
        ToolBar bar = new ToolBar();
        bar.setMinWidth(SCENE_WIDTH);
        bar.setLayoutY(SCENE_HEIGHT - 50);
        bar.setMinHeight(50);
        Button button = new Button("Load Image");

        TextField field = new TextField(DEFAULT_FILE);
        field.setMinWidth(SCENE_WIDTH - 130 - button.getWidth());
        bar.getItems().addAll(button, field);
        primaryStage.setTitle("Lab3: JavaFX animation and *.bmp operations");
        primaryStage.setResizable(true);

        Group root = new Group();
        root.getChildren().add(bar);
        Scene scene = new Scene(root, SCENE_WIDTH - 10, SCENE_HEIGHT);
        primaryStage.setScene(scene);
        primaryStage.show();
        Path path = new Path();
        Path pathNose = new Path();
        Path pathHead = new Path();
        Path pathEye = new Path();

        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try{
                    if(field.getText() == null || field.getText().length() == 0){
                        ReadingImageFromFile.loadBitmapImage(DEFAULT_FILE);
                    }
                    else{
                        ReadingImageFromFile.loadBitmapImage(field.getText());
                    }
                    image = ReadingImageFromFile.pr.image;
                    DrawImageFromBMP(root);
                }catch(Exception e){
                }
                root.getChildren().addAll(duck.getAll());
                for(int i = list.size() - 1; i > 1 ; i--){
                    path.getElements().addAll(
                            new MoveTo(list.get(i).x + duck.body.getPoints().get(0), list.get(i).y+ duck.body.getPoints().get(1)),
                            new LineTo (list.get(i - 1).x + duck.body.getPoints().get(0), list.get(i - 1).y + duck.body.getPoints().get(1))
                    );
                    pathNose.getElements().addAll(
                            new MoveTo(list.get(i).x + duck.nose.getPoints().get(0), list.get(i).y + duck.nose.getPoints().get(1)),
                            new LineTo (list.get(i - 1).x+ duck.nose.getPoints().get(0), list.get(i - 1).y + duck.nose.getPoints().get(1))
                    );
                    pathHead.getElements().addAll(
                            new MoveTo(list.get(i).x + duck.head.getCenterX(),list.get(i).y + duck.head.getCenterY() - 25),
                            new LineTo (list.get(i - 1).x+ duck.head.getCenterX(), list.get(i - 1).y + duck.head.getCenterY() - 25)
                    );
                    pathEye.getElements().addAll(
                            new MoveTo(list.get(i).x + duck.eye.getCenterX(), list.get(i).y + duck.eye.getCenterY() - 25),
                            new LineTo (list.get(i - 1).x+ duck.eye.getCenterX(), list.get(i - 1).y + duck.eye.getCenterY() - 25)
                    );
                }

                RotateTransition  bodyAnimRotate =
                        new RotateTransition(Duration.millis(1100), duck.body);
                bodyAnimRotate.setByAngle(-12f);
                bodyAnimRotate.setCycleCount(Timeline.INDEFINITE);
                bodyAnimRotate.setAutoReverse(true);



                ParallelTransition parallelTransition = new ParallelTransition();
                parallelTransition.getChildren().addAll(
                        generatePathTransition(duck.eye, pathEye),
                        generatePathTransition(duck.eye2, pathEye),
                        generatePathTransition(duck.nose, pathNose),
                        generatePathTransition(duck.head, pathHead),
                        generatePathTransition(duck.body, path),
                        bodyAnimRotate,
                        generateScaleTransition(duck.eye),
                        generateScaleTransition(duck.eye2),
                        generateScaleTransition(duck.nose),
                        generateScaleTransition(duck.head),
                        generateScaleTransition(duck.body)

                );
                parallelTransition.play();
            }
        });
    }

    private ScaleTransition generateScaleTransition(Shape shape){
        ScaleTransition scaleTransition =
                new ScaleTransition(Duration.seconds(18.0), shape);
        scaleTransition.setToX(1.5f);
        scaleTransition.setToY(1.5f);
        scaleTransition.setCycleCount(Timeline.INDEFINITE);
        return scaleTransition;
    }

    private PathTransition generatePathTransition(Shape shape, Path path)
    {
        PathTransition pathTransition = new PathTransition();
        pathTransition.setDuration(Duration.seconds(18.0));
        pathTransition.setPath(path);
        pathTransition.setNode(shape);
        pathTransition.setCycleCount(Timeline.INDEFINITE);
        return pathTransition;
    }

    private void DrawImageFromBMP(Group root)throws Exception {
        int width = (int) this.image.getWidth() - 10;
        int height = (int) this.image.getHeight();
        int half = (int) image.getHalfOfWidth();
        Circle cir;
        int let = 0;
        int let1 = 0;
        int let2 = 0;
        this.map = new char[width][height];
        BufferedInputStream reader = new BufferedInputStream(new FileInputStream("pixels.txt"));
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < half; j++) {
                let = reader.read();
                let1 = let;
                let2 = let;
                let1 = let1 & (0xf0);
                let1 = let1 >> 4;
                let2=let2&(0x0f);

                if(j * 2 < width){
                    cir = new Circle(j * 2, height - 1 - i, 1, Color.valueOf(returnPixelColor(let1)));
                    root.getChildren().add(cir);
                    if (returnPixelColor(let1) == "BLACK") {
                        map[j * 2][height - 1 - i] = '1';
                        list.add(new Point(j * 2, height - 1 - i));
                        numberOfPixels++;
                    } else {
                        map[j * 2][height - 1 - i] = '0';
                    }
                }

                if(j * 2 + 1 < width){
                    cir = new Circle(j * 2 + 1, height - 1 - i, 1, Color.valueOf(returnPixelColor(let2)));
                    root.getChildren().add(cir);
                    if (returnPixelColor(let2) == "BLACK") {
                        map[j * 2 + 1][height - 1 - i] = '1';
                        list.add(new Point(j * 2 + 1, height - 1 - i));
                        numberOfPixels++;
                    } else {
                        map[j * 2 + 1][height - 1 - i] = '0';
                    }
                }

            }
        }
        reader.close();

    }

    private String returnPixelColor(int color) {
        String col = "BLACK";
        switch (color) {
            case 0:
                return "BLACK";
            case 1:
                return "LIGHTCORAL";
            case 2:
                return "GREEN";
            case 3:
                return "BROWN";
            case 4:
                return "BLUE";
            case 5:
                return "MAGENTA";
            case 6:
                return "CYAN";
            case 7:
                return "LIGHTGRAY";
            case 8:
                return "DARKGRAY";
            case 9:
                return "RED";
            case 10:
                return "LIGHTGREEN";
            case 11:
                return "YELLOW";
            case 12:
                return "LIGHTBLUE";
            case 13:
                return "LIGHTPINK";
            case 14:
                return "LIGHTCYAN";
            case 15:
                return "WHITE";
        }
        return col;
    }

    public static void main(String args[]) {
        launch(args);
    }
}

