import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polygon;

import java.util.ArrayList;

/**
 * Created by yevhenii on 28.04.16.
 */
public class Duck{
    public Circle eye = new Circle(180,60,10);
    public Circle eye2 = new Circle(180,56,5);
    public Circle head = new Circle(170,80,70);

    public Polygon body = new Polygon(113,130,108,126,101,121,95,116,88,109,87,106,78,99,76,93,70,90,65,88,58,87,54,87,
            49,90,45,92,43,96,42,99,40,103,38,106,36,115,34,121,32,128,28,132,28,136,25,140,21,150,19,159,17,164,
            16,173,16,182,14,191,16,196,16,205,17,216,17,222,19,227,22,235,24,239,27,244,31,249,36,254,41,256,55,
            259,63,262,71,266,88,268,102,271,119,275,130,276,142,275,154,276,170,279,188,281,213,277,226,276,243,
            270,258,263,266,257,274,249,283,235,284,225,287,207,288,192,284,182,278,164,265,153,257,145,244,139);
    public Polygon nose = new Polygon(234,55,236,58,238,60,239,60,240,59,277,57,280,57,284,58,287,61,288,66,286,74,277,79,
            273,82,265,85,262,89,260,99,263,103,265,109,258,113,251,114,241,117,226,118,214,118,205,116,198,110,194,104,195,95,215,84);

    public Duck(){
        eye.setFill(Color.BLACK);
        eye2.setFill(Color.WHITE);
        head.setFill(Color.YELLOW);
        nose.setFill(Color.ORANGE);
        body.setFill(Color.YELLOW);
        body.setSmooth(true);
    }

    public ArrayList<Node> getAll() {
        ArrayList<Node> list = new ArrayList<Node>();
        list.add(head);
        list.add(body);
        list.add(nose);
        list.add(eye);
        list.add(eye2);
        return list;

    }
}
