package lab1;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.shape.*;
import javafx.scene.paint.Color;

public class Main extends Application {
    private static final int SCENE_WIDTH = 300;
    private static final int SCENE_HEIGHT = 250;

    @Override
    public void start(Stage primaryStage) throws Exception{
        System.out.println(getParameters());
        //Setting title of our main window
        primaryStage.setTitle("Lab1: JavaFX graphical primitives");

        //An ObservativeList instance of children that are rendered in order
        Group root = new Group();

        //arrays of c0ordinates for drawing a star
        double xpoints[] = {30, 105, 130, 155, 230, 180,
                190, 130, 70, 80};
        double ypoints[] = {80, 80, 10, 80, 80, 125,
                190, 150, 190, 125};

        //creating a star
        Polygon star = new Polygon();
        star.setFill(Color.YELLOW);
        for(int i = 0; i < xpoints.length; i++){
            star.getPoints().add(xpoints[i]);
            star.getPoints().add(ypoints[i]);
        }

        //creating a 5-eck inside-polygon
        Polygon insidePolygon = new Polygon();
        insidePolygon.setFill(Color.rgb(0, 128, 255));
        for(int i = 1; i < xpoints.length; i+=2){
            insidePolygon.getPoints().add(xpoints[i]);
            insidePolygon.getPoints().add(ypoints[i]);
        }

        //creating line and setting its color and width
        Line line = new Line(130, 150, 130, 220);
        line.setStroke(Color.YELLOW);
        line.setStrokeWidth(5);

        //adding created Shapes to the ObservativeList
        root.getChildren().addAll(line, star, insidePolygon);

        //setting up scene and showing it
        primaryStage.setScene(new Scene(root, SCENE_WIDTH, SCENE_HEIGHT, Color.RED));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
