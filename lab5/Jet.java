import com.sun.j3d.loaders.Scene;
import com.sun.j3d.loaders.objectfile.ObjectFile;
import com.sun.j3d.utils.image.TextureLoader;

import javax.media.j3d.*;
import javax.vecmath.Color3f;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;

/**
 * Created by yevhenii on 21.05.16.
 */
public class Jet {
    private Shape3D shapeJet;
    private TransformGroup jetTransform;
    private double sizeCoeff = 0.05d;
    private Transform3D translation;
    private Transform3D rotation;
    private Transform3D rotationAcse;
    private Transform3D rotationZAcse;
    private Transform3D scale;
    private double rotate = 90;
    private double acseRotate = 0;

    public double getAcseZRotate() {
        return acseZRotate;
    }

    public void setAcseZRotate(double acseZRotate) {
        this.acseZRotate = acseZRotate;
    }

    private double acseZRotate = 0;
    public Vector3d getPosition() {
        return position;
    }

    public void setPosition(Vector3d position) {
        this.position = position;
    }

    private Vector3d position = new Vector3d(-0.9f, -0.5f, 0.0f);

    public Vector3d getDirection() {
        return direction;
    }

    public void setDirection(Vector3d direction) {
        this.direction = direction;
    }

    private Vector3d direction = new Vector3d(0.01f, 0.01f, 0.01f);

    public Jet(){
        try{
            Scene scene = loadScene("model/jet.obj");
            listSceneNamedObjects(scene);
            Shape3D shape = (Shape3D)scene.getNamedObjects().get("jet");
            scene.getSceneGroup().removeChild(shape);
            shapeJet = shape;
        }
        catch(IOException e){
            System.out.println(e.getCause());
        }

        jetTransform = new TransformGroup();
        jetTransform.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        jetTransform.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);

        scale = new Transform3D();
        scale.setScale(sizeCoeff);

        Transform3D transform = new Transform3D();
        rotation = new Transform3D();
        rotationAcse = new Transform3D();
        rotationZAcse = new Transform3D();

        translation = new Transform3D();
        translation.setTranslation(position);

        transform.mul(translation);
        transform.mul(scale);
        jetTransform.setTransform(transform);
        shapeJet.setAppearance(getAppearance());
        jetTransform.addChild(shapeJet);
    }

    public void move(){
        scale.setScale(sizeCoeff);
        rotation.rotZ(- Math.PI/2 + Math.toRadians(rotate));
        rotationAcse.rotY(Math.toRadians(acseRotate));
        rotationZAcse.rotX(Math.toRadians(acseZRotate));
        position = new Vector3d(
                position.x + direction.x * Math.cos(Math.toRadians(rotate)) *  Math.cos(Math.toRadians(acseZRotate)),
                position.y + direction.y * Math.sin(Math.toRadians(rotate))  *  Math.cos(Math.toRadians(acseZRotate)),
                position.z + direction.z * Math.cos(Math.toRadians(acseZRotate)) * Math.sin(Math.toRadians(acseZRotate)));

        sizeCoeff += Math.sin(Math.toRadians(acseZRotate)) * 0.0001;

        translation.setTranslation(position);
        Transform3D trans = new Transform3D();
        trans.mul(scale);
        trans.mul(translation);
        trans.mul(rotation);
        trans.mul(rotationAcse);
        trans.mul(rotationZAcse);
        jetTransform.setTransform(trans);
    }
    public void height(double step){
        position.z += step;
        sizeCoeff += Math.signum(step) * 0.05;
    }

    public void onAcse(double grad){
        acseRotate += grad;
    }

    public void onAcseZ(double grad){
        acseZRotate += grad;
    }


    public void rotate(double grad){
        rotate += grad;
        return;
    }



    private double getScalarProduct(Vector3d a, Vector3d b){
        return a.x * b.x + a.y * b.y + a.z * b.z;
    }

    private double getVectorLength(Vector3d a){
        return Math.sqrt(a.x * a.x + a.y * a.y + a.z * a.z);
    }


    private Appearance getAppearance(){
        TextureLoader texture;
        texture = new TextureLoader("jet_texture.jpg", "LUMINANCE", null);
        Background background = new Background(texture.getImage());
        background.setImageScaleMode(Background.SCALE_FIT_ALL);
        Material material = new Material();
        material.setAmbientColor ( new Color3f( 0, 0, 0 ) );
        material.setDiffuseColor ( new Color3f( 0, 0, 0 ) );
        material.setSpecularColor( new Color3f( 0, 0, 0) );
        material.setShininess( 0.3f );
        material.setLightingEnable(true);
        TextureAttributes texAttr = new TextureAttributes();
        texAttr.setTextureMode(TextureAttributes.REPLACE);
        Appearance app = new Appearance();
        app.setMaterial(material);
        app.setTextureAttributes(texAttr);
        app.setTexture(texture.getTexture());
        return app;
    }

    private static Scene loadScene(String location) throws IOException{
        ObjectFile loader = new ObjectFile(ObjectFile.RESIZE);
        return loader.load(new FileReader(location));
    }

    private void listSceneNamedObjects(Scene scene){
        if(scene == null){
            return;
        }
        Map<String, Shape3D> nameMap = scene.getNamedObjects();

        for(String name : nameMap.keySet()){
            System.out.printf("Name: %s\n", name);
        }
    }


    public Shape3D getShapeJet() {
        return shapeJet;
    }

    public void setShapeJet(Shape3D shapeJet) {
        this.shapeJet = shapeJet;
    }

    public TransformGroup getJetTransform() {
        return jetTransform;
    }

    public void setJetTransform(TransformGroup jetTransformGroup) {
        this.jetTransform = jetTransformGroup;
    }

}
