import com.sun.j3d.utils.image.TextureLoader;
import com.sun.j3d.utils.universe.SimpleUniverse;

import javax.media.j3d.*;
import javax.swing.*;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Created by yevhenii on 21.05.16.
 */
public class DemoScene extends JFrame implements KeyListener, ActionListener {

    private static SimpleUniverse universe;
    private static BranchGroup root;
    private static Canvas3D canvas;
    private static TransformGroup backgroundTransformation;
    private static Jet jet;
    private static boolean KEY_PRESSED = false;

    public DemoScene(String title) {
        super(title);
        Timer timer = new Timer(4, this);
        timer.start();
        getContentPane().setLayout(new BorderLayout());
        canvas = new Canvas3D(SimpleUniverse.getPreferredConfiguration());
        canvas.addKeyListener(this);
        getContentPane().add("Center", canvas);
        universe = new SimpleUniverse(canvas);
        root = new BranchGroup();
        root.addChild(getLightToUniverse());
        jetInit();
        initEarth();
        root.addChild(backgroundTransformation);
        universe.getViewingPlatform().setNominalViewingTransform();
        universe.addBranchGraph(root);
    }

    private void jetInit() {
        jet = new Jet();
        root.addChild(jet.getJetTransform());
    }

    private DirectionalLight getLightToUniverse() {
        Bounds bounds = new BoundingSphere();
        Color3f color = new Color3f(255f / 255f, 255 / 255f, 255 / 255f);
        Vector3f lightDirection = new Vector3f(0f, 0f, -1f);
        DirectionalLight dirLight = new DirectionalLight(color, lightDirection);
        dirLight.setInfluencingBounds(bounds);
        return dirLight;
    }

    private void initEarth() {
        TextureLoader texture;
        texture = new TextureLoader("earth.jpg", canvas);
        Background background = new Background(texture.getImage());
        background.setImageScaleMode(Background.SCALE_FIT_ALL);
        BoundingSphere bounds = new BoundingSphere(new Point3d(0.0, 0.0, 0.0), 100.0);
        background.setApplicationBounds(bounds);
        root.addChild(background);
    }

    @Override
    public void keyPressed(KeyEvent e) {
        Vector3d dir;
        switch (e.getKeyChar()) {
            case 'w':
                jet.onAcseZ(2.0d);
                break;
            case 'a':
                jet.rotate(2.0d);
                break;
            case 's':
                jet.onAcseZ(-2.0d);
                break;
            case 'd':
                jet.rotate(-2.0d);
                break;
            case 'q':
                jet.onAcse(2.0d);
                break;
            case 'e':
                jet.onAcse(-2.0d);
                break;
        }
   }


    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    public void actionPerformed(ActionEvent e) {
        if(jet == null){
            return;
        }
        jet.move();
    }
}
