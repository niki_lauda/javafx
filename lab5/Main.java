import javax.swing.*;

/**
 * Created by yevhenii on 21.05.16.
 */

public class Main {
    public static void main(String[] args) {
        DemoScene scene = new DemoScene("Yevhenii Maliavka Lab 5");
        scene.setSize(1350, 760);
        scene.setVisible(true);
        scene.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }
}
